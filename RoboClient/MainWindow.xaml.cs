﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ServiceModel;
using System.ServiceModel.Channels;
using RoboAdmin;

namespace RoboClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// 

    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            ChannelFactory<IRoboInterface> pipeFactory = new ChannelFactory<IRoboInterface>(new NetNamedPipeBinding(), new EndpointAddress("net.pipe://localhost/ololololololololololololololo"));
            IRoboInterface pipeProxy = pipeFactory.CreateChannel();
            DaButton.Content = pipeProxy.RequestInstall("test");
        }
    }
}
