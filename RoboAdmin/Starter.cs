﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Principal;
using System.Diagnostics;
using System.Windows.Forms;
using System.Configuration.Install;
using System.ServiceProcess;

namespace RoboAdmin
{
    class Starter
    {
        static class Program
        {
            static String ServiceName = "RoboAdmin";
            static String ServiceDisplayName = "AWESOME ROBO ADMIN";
            /// <summary>
            /// The main entry point for the application.
            /// </summary>
            static void Main(string[] args)
            {
                if (args.Length > 0)
                {
                    if (args[0] == "install" || args[0] == "remove" || args[0] == "remove" || args[0] == "delete")
                    {
                        Elevate(args);
                    }
                    else if (args[0] == "elevated")
                    {
                        if (RunningAsAdmin())
                        {
                            if (args[1] == "install")
                            {
                                
                                try
                                {
                                    ServiceController sc = new ServiceController(ServiceName);
                                    Console.WriteLine("Service already installed, updating...");
                                    RemoveService();
                                    InstallService();
                                }
                                catch
                                {
                                    InstallService();
                                }                              
                            }
                            else if (args[1] == "remove" || args[1] == "remove" || args[1] == "delete") 
                            {
                                RemoveService();
                            }
                            else
                            {
                                PrintAndExit("ERROR  Unknown request, nothing to do.", 1);
                            }
                            Console.WriteLine("");
                            Console.WriteLine("All done. Press any key to exit.");
                            Console.ReadKey();
                            
                        }
                        else
                        {
                            PrintAndExit("ERROR Could not get elevated privileges.", 1);
                        }
                    }
                    else
                    {
                        PrintAndExit("ERROR Unknown request, nothing to do.", 1);
                    }
                }
                else
                    System.ServiceProcess.ServiceBase.Run(new RoboAdminService());
            }

            private static bool RunningAsAdmin()
            {
                var Principle = new WindowsPrincipal(WindowsIdentity.GetCurrent());
                return Principle.IsInRole(WindowsBuiltInRole.Administrator);
            }

            private static bool Elevate(string[] args)
            {
                Process proc = new Process
                {
                    StartInfo = new ProcessStartInfo
                    {
                        UseShellExecute = true, // required for elevation
                        WorkingDirectory = Environment.CurrentDirectory,
                        FileName = Application.ExecutablePath,
                        Verb = "runas",
                        Arguments = "elevated " + String.Join(" ", args)
                    }
                };
                try
                {
                    proc.Start();
                    proc.WaitForExit();
                    if (proc.ExitCode == 0)
                    {
                        Console.WriteLine("Elevated operation succeeded");
                    }
                    else
                    {
                        Console.WriteLine("Elevated operation failed");
                    }
                    return true;
                }
                catch
                {
                    Console.WriteLine("Unable to elevate.");
                    return false;
                }
            }

            private static void PrintAndExit(string message, int exitCode)
            {
                Console.WriteLine(message);
                Console.ReadKey();
                Environment.Exit(1);
            }

            private static void RemoveService()
            {
                Console.WriteLine("");
                Console.WriteLine("START    Running removal procedure...");
                Console.WriteLine("1    Stopping service...");
                try
                {
                    ServiceController sc = new ServiceController(ServiceName);
                    if (sc.Status != ServiceControllerStatus.Stopped)
                    {
                        try
                        {
                            sc.Stop();
                            Console.WriteLine("1.DONE Service stopped.");
                        }
                        catch
                        {
                            PrintAndExit("ERROR    Service could not be stopped.", 1);
                        }
                    }
                    else
                        Console.WriteLine("1.DONE Service is already stopped.");
                    Console.WriteLine("2    Removing service...");
                    Process proc = new Process
                    {
                        StartInfo = new ProcessStartInfo
                        {
                            UseShellExecute = false,
                            FileName = "sc",
                            Arguments = "delete " + ServiceName
                        }
                    };
                    try
                    {
                        proc.Start();
                        proc.WaitForExit();
                        if (proc.ExitCode == 0)
                        {
                            Console.WriteLine("2.DONE   Service removed.");
                            Console.WriteLine("END  Service removed successfully.");
                        }
                        else
                        {
                            PrintAndExit("ERROR Could not remove service.", 1);
                        }
                    }
                    catch
                    {
                        PrintAndExit("ERROR Service configuration failed.", 1);
                    }
                }
                catch
                {
                    Console.WriteLine("END  Service was not found. Nothing to do.");
                }
            }

            private static void InstallService()
            {
                Console.WriteLine("");
                Console.WriteLine("START    Running installation procedure...");
                Console.WriteLine("1    Installing service...");
                String capPath = System.Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
                String exeFull = System.Reflection.Assembly.GetExecutingAssembly().Location;
                String exeName = System.IO.Path.GetFileName(exeFull);
                String appPath = capPath + "\\" + ServiceName;
                String newFull = appPath + "\\" + exeName;
                Console.WriteLine("1.1   Creating service directory and files in " + capPath + "...");
                if (!System.IO.Directory.Exists(appPath))
                {
                    Console.WriteLine("1.1.1  Creating " + ServiceName + " directory in " + appPath + "...");
                    System.IO.Directory.CreateDirectory(appPath);
                    Console.WriteLine("1.1.DONE  Directory created.");
                }
                else
                {
                    Console.WriteLine("1.1.DONE  Directory " + ServiceName + " already present in " + appPath + ", skipping...");
                }
                Console.WriteLine("1.2    Copying executable to " + appPath + "...");
                if (System.IO.File.Exists(newFull))
                {
                    Console.WriteLine("1.2.INFO  File already present, updating...");
                }
                try
                {
                    System.IO.File.Copy(exeFull, newFull, true);
                    Console.WriteLine("1.2.DONE  Executable deployed.");
                }
                catch
                {
                    PrintAndExit("ERROR  Operation failed", 1);
                }
                Console.WriteLine("1.3   Configuring service...");
                Process proc = new Process
                {
                    StartInfo = new ProcessStartInfo
                    {
                        UseShellExecute = false,
                        WorkingDirectory = appPath,
                        FileName = "sc",
                        Arguments = "create " + ServiceName +
                                    " binpath= \"" + newFull + "\"" +
                                    " displayname= \"" + ServiceDisplayName + "\"" +
                                    " start= \"auto\""
                    }
                };
                try
                {
                    proc.Start();
                    proc.WaitForExit();
                    if (proc.ExitCode == 0)
                    {
                        Console.WriteLine("2    Starting service...");
                        ServiceController sc = new ServiceController(ServiceName);
                        try
                        {
                            sc.Start();
                            Console.WriteLine("3.1  Service installed, starting...");
                        }
                        catch
                        {
                            PrintAndExit("ERROR Could not start service.", 1);
                        }
                    }
                    else
                    {
                        PrintAndExit("ERROR    Could not install service.", 1);
                    }
                }
                catch
                {
                    PrintAndExit("ERROR    Service configuration failed.", 1);
                }
                Console.WriteLine("END  Service successfully configured.");
            }
        }
    }
}
