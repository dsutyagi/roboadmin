﻿using System;
using System.ServiceProcess;
using System.ServiceModel;

namespace RoboAdmin
{
    [ServiceContract]
    public interface IRoboInterface
    {
        [OperationContract]
        bool RequestInstall(string value);
    }

    public class RoboInterface : IRoboInterface
    {
        public bool RequestInstall(string value)
        {
            return true;
        }
    }

    class RoboAdminService : ServiceBase
    {
        String configFile = "roboadmin.conf";
        String configFolder = "roboadmin";

        String fullPathToConfig;

        ServiceHost host;

        public RoboAdminService()
        {
            this.CanStop = true;
            this.CanPauseAndContinue = false;
            this.AutoLog = true;
        }

        protected override void OnStart(string[] args)
        {
            host = new ServiceHost(typeof(RoboInterface),new Uri[]{ new Uri("net.pipe://localhost") });
            host.AddServiceEndpoint(typeof(IRoboInterface),new NetNamedPipeBinding(),"ololololololololololololololo");
            host.Open();
        }

        protected override void OnStop()
        {
            host.Close();
        }

    }
}